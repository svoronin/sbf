package com.softserveinc.ita.svoronin.hotel.repository;

import com.softserveinc.ita.svoronin.hotel.entity.Visitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

public interface VisitorRepository extends JpaRepository<Visitor, Integer> {

    @RestResource(exported = false)
    @Override
    void delete(Integer integer);
}
