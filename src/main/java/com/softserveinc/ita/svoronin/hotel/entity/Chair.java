package com.softserveinc.ita.svoronin.hotel.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Chair {

    @Id
    @GeneratedValue
    private Integer id;
    private String color;

    Chair() {}

    public Chair(String color) {
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Chair{" +
                "id=" + id +
                ", color='" + color + '\'' +
                '}';
    }
}
