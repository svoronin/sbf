package com.softserveinc.ita.svoronin.hotel.repository;

import com.softserveinc.ita.svoronin.hotel.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface RoomRepository extends JpaRepository<Room, Integer> {

    Room findByNumber(@Param("number") String number);

//    @Query("select * from room where number = :number and price = :price")
    Room findByPriceAndNumber(
            @Param("number") String number,
            @Param("price") Integer price
    );
}
