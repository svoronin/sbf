package com.softserveinc.ita.svoronin.hotel.config;

import com.softserveinc.ita.svoronin.hotel.resource.RoomResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig () {
        register(RoomResource.class);
    }
}
