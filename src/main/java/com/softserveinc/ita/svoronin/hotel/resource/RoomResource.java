package com.softserveinc.ita.svoronin.hotel.resource;

import com.softserveinc.ita.svoronin.hotel.entity.Room;
import com.softserveinc.ita.svoronin.hotel.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.Collection;

@Path("/jersey/room")
@Produces("application/json")
public class RoomResource {

    private RoomRepository roomRepository;

    @Autowired
    public RoomResource(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @GET
    public Collection<Room> getAllRooms(){
        return roomRepository.findAll();
    }

    @GET
    @Path("/{number}")
    public Room getRoom(@PathParam("number") String number) {
        return roomRepository.findByNumber(number);
    }

}
