package com.softserveinc.ita.svoronin.hotel.entity;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Room extends ResourceSupport {

    @Id
    @GeneratedValue
    private Integer id;
    private String number;
    private Integer price;
    @OneToMany(fetch = FetchType.EAGER)
    private List<Chair> chairs;

    Room() {}

    public Room(String number, Integer price, List<Chair> chairs) {
        this.number = number;
        this.price = price;
        this.chairs = chairs;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<Chair> getChairs() {
        return chairs;
    }

    public void setChairs(List<Chair> chairs) {
        this.chairs = chairs;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", price=" + price +
                ", chairs=" + chairs +
                '}';
    }
}
