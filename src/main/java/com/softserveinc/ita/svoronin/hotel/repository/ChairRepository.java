package com.softserveinc.ita.svoronin.hotel.repository;

import com.softserveinc.ita.svoronin.hotel.entity.Chair;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChairRepository extends JpaRepository<Chair, Integer> {
}
