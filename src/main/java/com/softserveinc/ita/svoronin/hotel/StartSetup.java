package com.softserveinc.ita.svoronin.hotel;

import com.softserveinc.ita.svoronin.hotel.entity.Chair;
import com.softserveinc.ita.svoronin.hotel.entity.Room;
import com.softserveinc.ita.svoronin.hotel.repository.ChairRepository;
import com.softserveinc.ita.svoronin.hotel.repository.RoomRepository;
import com.softserveinc.ita.svoronin.hotel.repository.VisitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Stream;

@Component
public class StartSetup implements CommandLineRunner {

    private RoomRepository roomRepository;
    private ChairRepository chairRepository;
    private VisitorRepository visitorRepository;

    @Override
    public void run(String... strings) throws Exception {
        Stream.of("green", "red", "black", "yellow")
                .forEach(color -> chairRepository.save(new Chair(color)));

        roomRepository.save(new Room("1", 10,
                Arrays.asList(
                        chairRepository.findOne(1),
                        chairRepository.findOne(2))));
        roomRepository.save(new Room("2", 20,
                Arrays.asList(
                        chairRepository.findOne(3),
                        chairRepository.findOne(4))));
        roomRepository.findAll().forEach(System.out::println);
        System.out.println("EOF");
    }

    @Autowired
    public StartSetup(RoomRepository roomRepository, ChairRepository chairRepository, VisitorRepository visitorRepository) {
        this.roomRepository = roomRepository;
        this.chairRepository = chairRepository;
        this.visitorRepository = visitorRepository;
    }
}
