package com.softserveinc.ita.svoronin.hotel.controller;

import com.softserveinc.ita.svoronin.hotel.entity.Room;
import com.softserveinc.ita.svoronin.hotel.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class RoomController {

    private RoomRepository roomRepository;

    @Autowired
    public RoomController(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/room")
    public Collection<Room> getAllRooms(){
        List<Room> rooms = roomRepository.findAll();
        for (Room room : rooms) {
            room.add(
                    linkTo(
                            methodOn(RoomController.class)
                                    .getRoom(room.getNumber())
                    ).withSelfRel()
            );
        }
        return rooms;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/room/{number}")
    public Room getRoom(@PathVariable String number) {
        Room room = roomRepository.findByNumber(number);
        room.add(linkTo(
                methodOn(RoomController.class)
                        .getRoom(room.getNumber())
        ).withSelfRel());
        return room;
    }
}
